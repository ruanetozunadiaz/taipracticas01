function buscarPorId() {
    const id = document.getElementById("txtNumeroId").value;

    const url = "https://jsonplaceholder.typicode.com/users/" + id;
    
    if (id.trim() === "") {
        alert("Por favor, ingresa un ID");
        return;
    }

    const http = new XMLHttpRequest();

    http.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                const datos = JSON.parse(this.responseText);
                // Mostrar el título en el elemento con id "txtTitulo"
                document.getElementById("txtName").value = datos.name;
                document.getElementById("txtUsername").value = datos.username;
                document.getElementById("txtEmail").value = datos.email;
                document.getElementById("txtStreet").value = datos.address.street;
                document.getElementById("txtSuite").value = datos.address.suite;
                document.getElementById("txtCity").value = datos.address.city;
                document.getElementById("txtZipcode").value = datos.address.zipcode;
                document.getElementById("txtLat").value = datos.address.geo.lat;
                document.getElementById("txtLng").value = datos.address.geo.lat;
                document.getElementById("txtPhone").value = datos.phone;
                document.getElementById("txtWebsite").value = datos.website;
                document.getElementById("txtCompanyName").value = datos.company.name;
                document.getElementById("txtCatchPhrase").value = datos.company.catchPhrase;
                document.getElementById("txtBs").value = datos.company.bs;
            } else {
                alert("Lo siento, no he encontrado el ID");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener("click", function() {
    buscarPorId();
});

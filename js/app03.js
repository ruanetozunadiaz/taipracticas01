function hacerPeticion(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users";

    // Validar la respuesta

    http.onreadystatechange = function(){
        if(this.status == 200 && this.readyState == 4){

            // Aqui se dibuja la pagina

            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            // Ciclo para ir tomando cada uno de los registros
            res.innerHTML = '';
            
            for(const datos of json){
                res.innerHTML += '<tr> <td class="columna1">' + datos.id + '</td>'
                + '<td class="columna2">' + datos.name + '</td>'
                + '<td class="columna3">' + datos.username + '</td>'
                + '<td class="columna4">' + datos.email + '</td>'
                + '<td class="columna5">' + datos.address.street + '</td>'
                + '<td class="columna6">' + datos.address.suite + '</td>'
                + '<td class="columna7">' + datos.address.city + '</td>'
                + '<td class="columna8">' + datos.address.zipcode + '</td>'
                + '<td class="columna9">' + datos.address.geo.lat + '</td>'
                + '<td class="columna10">' + datos.address.geo.lat + '</td>'
                + '<td class="columna11">' + datos.phone + '</td>'
                + '<td class="columna12">' + datos.website + '</td>'
                + '<td class="columna13">' + datos.company.name + '</td>'
                + '<td class="columna14">' + datos.company.catchPhrase + '</td>'
                + '<td class="columna15">' + datos.company.bs + '</td> </tr>';
            }
            res.innerHTML += "</tbody>"
        }
    }
    
    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener("click",function(){
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res = document.getElementById("lista");
    res.innerHTML="";
});
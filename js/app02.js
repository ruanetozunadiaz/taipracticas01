function buscarPorId() {
    const id = document.getElementById("txtNumeroId").value;

    const url = "https://jsonplaceholder.typicode.com/albums/" + id;
    
    if (id.trim() === "") {
        alert("Por favor, ingresa un ID");
        return;
    }

    const http = new XMLHttpRequest();

    http.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                const datos = JSON.parse(this.responseText);
                document.getElementById("txtTitulo").value = datos.title;
            } else {
                alert("Lo siento, no he encontrado el ID");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener("click", function() {
    buscarPorId();
});

const listaRazas = document.getElementById('plazos');
const imagenPerro = document.getElementById('imagenPerro');
const contenedorError = document.getElementById('error');

document.getElementById('btnCargar').addEventListener('click', async (e) => {
    e.preventDefault();
    limpiarMensajesError();
    const url = 'https://dog.ceo/api/breeds/list';

    try {
        const respuesta = await fetch(url);
        const datos = await respuesta.json();
        mostrarOpciones(datos);
    } catch (error) {
        mostrarMensajeError('No se encontraron datos');
    }
});

document.getElementById('btnVerImagen').addEventListener('click', () => {
    const razaSeleccionada = listaRazas.value;
    if (!razaSeleccionada) {
        mostrarMensajeError();
        return;
    }
    mostrarImagen(razaSeleccionada);
});

listaRazas.addEventListener('change', (e) => {
    mostrarImagen(e.target.value);
});

async function mostrarImagen(raza) {
    const url = `https://dog.ceo/api/breed/${raza}/images/random`;

    try {
        console.log('Recuperando:', url);
        const respuesta = await fetch(url);
        const datos = await respuesta.json();
        console.log('Resultado:', datos);
        mostrarImagenEnHTML(datos);
    } catch (error) {
        console.error('Error al recuperar la imagen:', error);
        mostrarMensajeError('No se encontró la IMAGEN');
    }
}

function mostrarOpciones(razasData) {
    limpiarListaDesplegable();
    razasData.message.forEach(raza => {
        const opcion = document.createElement('option');
        opcion.value = raza;
        opcion.innerText = raza;
        listaRazas.appendChild(opcion);
    });
}

function mostrarImagenEnHTML(datosImagen) {
    limpiarImagen();
    const imagenElemento = document.createElement('img');
    imagenElemento.src = datosImagen.message;
    imagenPerro.appendChild(imagenElemento);
}

function limpiarListaDesplegable() {
    while (listaRazas.firstChild) {
        listaRazas.removeChild(listaRazas.firstChild);
    }
}

function limpiarImagen() {
    while (imagenPerro.firstChild) {
        imagenPerro.removeChild(imagenPerro.firstChild);
    }
}

function limpiarMensajesError() {
    while (contenedorError.firstChild) {
        contenedorError.removeChild(contenedorError.firstChild);
    }
}

function mostrarMensajeError() {
    alert("Por favor, selecciona una mascota para mostrar su imagen");
}

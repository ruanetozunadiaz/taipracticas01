function buscarPorId() {
    const id = document.getElementById("txtNumeroId").value;

    const url = "https://jsonplaceholder.typicode.com/users/" + id;

    if (id.trim() === "") {
        alert("Por favor, ingresa un ID");
        return;
    }

    axios.get(url)
        .then(function (response) {

            const datos = response.data;

            document.getElementById("txtName").value = datos.name;
            document.getElementById("txtUsername").value = datos.username;
            document.getElementById("txtEmail").value = datos.email;
            document.getElementById("txtStreet").value = datos.address.street;
            document.getElementById("txtNumber").value = datos.address.suite;
            document.getElementById("txtCity").value = datos.address.city;
        })
        .catch(function (error) {
            if (error.response && error.response.status === 404) {
                alert("Lo siento, no he encontrado el ID");
            } else {
                console.error('Error al realizar la solicitud:', error);
            }
        });
}

document.getElementById("btnBuscar").addEventListener("click", function() {
    buscarPorId();
});

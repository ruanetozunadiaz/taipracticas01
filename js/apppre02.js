llamandoFetch = () => {
    const name = document.getElementById("txtCountry").value;

    const url = "https://restcountries.com/v3.1/name/" + name + "?fullText=true";

    if (name.trim() === "") {
        alert("Por favor, ingresa un nombre de país");
        return;
    }

    fetch(url)
        .then(respuesta => respuesta.json())
        .then(data => mostrarTodos(data))
        .catch((reject) => {
            console.log("Surgió un error" + reject);
        });
}

const mostrarTodos = (data) => {
    const item = data[0];

    const capital = item.capital?.[0] || 'N/A';

    const languages = Object.values(item.languages || {}).join(', ');

    document.getElementById("txtCapital").value = capital;
    document.getElementById("txtLanguage").value = languages || 'N/A';
}


document.getElementById("btnBuscar").addEventListener('click', function () {
    llamandoFetch();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    document.getElementById("txtCountry").value = "";
    document.getElementById("txtCapital").value = "";
    document.getElementById("txtLanguage").value = "";
});
